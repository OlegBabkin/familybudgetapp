namespace FamilyBudgetApp.Domain.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FamilyBudget.Transactions")]
    public partial class Transaction : BaseModel
    {
        public int Id { get; set; }

        private DateTime? date;
        public DateTime? Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }

        private int productId;
        public int ProductId
        {
            get { return productId; }
            set
            {
                productId = value;
                OnPropertyChanged("ProductId");
            }
        }

        private int transactionTypeId;
        public int TransactionTypeId
        {
            get { return transactionTypeId; }
            set
            {
                transactionTypeId = value;
                OnPropertyChanged("TransactionTypeId");
            }
        }

        private decimal? euroValue;
        [Column(TypeName = "numeric")]
        public decimal? EuroValue
        {
            get { return euroValue; }
            set
            {
                euroValue = value;
                OnPropertyChanged("EuroValue");
            }
        }

        private decimal? amount;
        [Column(TypeName = "numeric")]
        public decimal? Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                OnPropertyChanged("Amount");
            }
        }

        private decimal? summ;
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal? Summ
        {
            get { return summ; }
            set
            {
                summ = value;
                OnPropertyChanged("Summ");
            }
        }

        private int userAccountId;
        public int UserAccountId
        {
            get { return userAccountId; }
            set
            {
                userAccountId = value;
                OnPropertyChanged("UserAccountId");
            }
        }

        public virtual Product Product { get; set; }

        public virtual TransactionType TransactionType { get; set; }

        public virtual UserAccount UserAccount { get; set; }
    }
}
