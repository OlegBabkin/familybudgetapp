﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class AccountsRepository : IAccountsRepository
    {
        private AppDbContext context;

        public AccountsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(Account entity)
        {
            this.context.Accounts.Remove(entity);
        }

        public IEnumerable<Account> FindBy(Expression<Func<Account, bool>> predicate)
        {
            return this.context.Accounts.Where(predicate).ToList();
        }

        public IEnumerable<Account> GetAllEntries()
        {
            return this.context.Accounts;
        }

        public Account GetByKey(int key)
        {
            return this.context.Accounts.FirstOrDefault(a => a.Id == key);
        }

        public IEnumerable<UserAccount> GetUserAccountCollection(Account account)
        {
            return this.GetByKey(account.Id).UserAccounts;
        }

        public void Insert(Account entity)
        {
            this.context.Accounts.Add(entity);
        }

        public void Update(Account entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
