﻿using System;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class UnitOfWork : IUnitOfWork
    {
        private AppDbContext context;

        private IAccountsRepository accounts;
        private ICategoriesRepository categories;
        private IProductsRepository products;
        private ITransactionsRepository transactions;
        private ITransationTypesRepository transactionTypes;
        private IUserAccountsRepository userAccountCollection;
        private IUsersRepository users;

        public UnitOfWork(AppDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException("DB context is missing!");
        }

        public UnitOfWork(String connectionString)
        {
            this.context = new AppDbContext(connectionString);
        }

        public IAccountsRepository Accounts
        {
            get
            {
                if (accounts == null) { accounts = new AccountsRepository(context); }
                return accounts;
            }
        }

        public ICategoriesRepository Categories
        {
            get
            {
                if (categories == null) { categories = new CategoriesRepository(context); }
                return categories;
            }
        }

        public IProductsRepository Products
        {
            get
            {
                if (products == null) { products = new ProductsRepository(context); }
                return products;
            }
        }

        public ITransactionsRepository Transactions
        {
            get
            {
                if (transactions == null) { transactions = new TransactionsRepository(context); }
                return transactions;
            }
        }

        public ITransationTypesRepository TransactionTypes
        {
            get
            {
                if (transactionTypes == null) { transactionTypes = new TransationTypesRepository(context); }
                return transactionTypes;
            }
        }

        public IUserAccountsRepository UserAccountCollection
        {
            get
            {
                if (userAccountCollection == null) { userAccountCollection = new UserAccountsRepository(context); }
                return userAccountCollection;
            }
        }

        public IUsersRepository Users
        {
            get
            {
                if (users == null) { users = new UsersRepository(context); }
                return users;
            }
        }

        public void Save()
        {
            this.context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
    