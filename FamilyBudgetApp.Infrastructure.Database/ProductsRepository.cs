﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class ProductsRepository : IProductsRepository
    {
        private AppDbContext context;

        public ProductsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(Product entity)
        {
            this.context.Products.Remove(entity);
        }

        public IEnumerable<Product> FindBy(Expression<Func<Product, bool>> predicate)
        {
            return this.context.Products.Where(predicate).ToList();
        }

        public IEnumerable<Product> GetAllEntries()
        {
            return this.context.Products;
        }

        public Product GetByKey(int key)
        {
            return this.context.Products.FirstOrDefault(p => p.Id == key);
        }

        public IEnumerable<Transaction> GetProductTransactions(Product product)
        {
            return this.GetByKey(product.Id).Transactions;
        }

        public void Insert(Product entity)
        {
            this.context.Products.Add(entity);
        }

        public void Update(Product entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
