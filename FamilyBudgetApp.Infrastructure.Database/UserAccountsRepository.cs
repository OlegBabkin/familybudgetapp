﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class UserAccountsRepository : IUserAccountsRepository
    {
        private AppDbContext context;

        public UserAccountsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(UserAccount entity)
        {
            this.context.UserAccounts.Remove(entity);
        }

        public IEnumerable<UserAccount> FindBy(Expression<Func<UserAccount, bool>> predicate)
        {
            return this.context.UserAccounts.Where(predicate).ToList();
        }

        public IEnumerable<UserAccount> GetAllEntries()
        {
            return this.context.UserAccounts;
        }

        public UserAccount GetByKey(int key)
        {
            return this.context.UserAccounts.FirstOrDefault(ua => ua.Id == key);
        }

        public IEnumerable<Transaction> GetUserAccountTransactions(UserAccount userAccount)
        {
            return this.GetByKey(userAccount.Id).Transactions;
        }

        public void Insert(UserAccount entity)
        {
            this.context.UserAccounts.Add(entity);
        }

        public void Update(UserAccount entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
