﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class TransactionsRepository : ITransactionsRepository
    {
        private AppDbContext context;

        public TransactionsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(Transaction entity)
        {
            this.context.Transactions.Remove(entity);
        }

        public IEnumerable<Transaction> FindBy(Expression<Func<Transaction, bool>> predicate)
        {
            return this.context.Transactions.Where(predicate).ToList();
        }

        public IEnumerable<Transaction> GetAllEntries()
        {
            return this.context.Transactions;
        }

        public Transaction GetByKey(int key)
        {
            return this.context.Transactions.FirstOrDefault(t => t.Id == key);
        }

        public void Insert(Transaction entity)
        {
            this.context.Transactions.Add(entity);
        }

        public void Update(Transaction entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
