﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class UsersRepository : IUsersRepository
    {
        private AppDbContext context;

        public UsersRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(User entity)
        {
            this.context.Users.Remove(entity);
        }

        public IEnumerable<User> FindBy(Expression<Func<User, bool>> predicate)
        {
            return this.context.Users.Where(predicate).ToList();
        }

        public IEnumerable<User> GetAllEntries()
        {
            return this.context.Users;
        }

        public User GetByKey(int key)
        {
            return this.context.Users.FirstOrDefault(u => u.Id == key);
        }

        public IEnumerable<UserAccount> GetUserAccountCollection(User user)
        {
            return this.GetByKey(user.Id).UserAccounts;
        }

        public void Insert(User entity)
        {
            this.context.Users.Add(entity);
        }

        public void Update(User entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
