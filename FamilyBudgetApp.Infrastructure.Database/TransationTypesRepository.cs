﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class TransationTypesRepository : ITransationTypesRepository
    {
        private AppDbContext context;

        public TransationTypesRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(TransactionType entity)
        {
            this.context.TransactionTypes.Remove(entity);
        }

        public IEnumerable<TransactionType> FindBy(Expression<Func<TransactionType, bool>> predicate)
        {
            return this.context.TransactionTypes.Where(predicate).ToList();
        }

        public IEnumerable<TransactionType> GetAllEntries()
        {
            return this.context.TransactionTypes;
        }

        public TransactionType GetByKey(int key)
        {
            return this.context.TransactionTypes.FirstOrDefault(tt => tt.Id == key);
        }

        public IEnumerable<Transaction> GetTypeTrunsactions(TransactionType type)
        {
            return this.GetByKey(type.Id).Transactions;
        }

        public void Insert(TransactionType entity)
        {
            this.context.TransactionTypes.Add(entity);
        }

        public void Update(TransactionType entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
