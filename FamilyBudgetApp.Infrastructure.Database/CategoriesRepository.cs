﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Infrastructure.Database
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private AppDbContext context;

        public CategoriesRepository(AppDbContext context)
        {
            this.context = context;
        }

        public void Delete(Category entity)
        {
            this.context.Categories.Remove(entity);
        }

        public IEnumerable<Category> FindBy(Expression<Func<Category, bool>> predicate)
        {
            return this.context.Categories.Where(predicate).ToList();
        }

        public IEnumerable<Category> GetAllEntries()
        {
            return this.context.Categories;
        }

        public Category GetByKey(int key)
        {
            return this.context.Categories.FirstOrDefault(c => c.Id == key);
        }

        public IEnumerable<Product> GetCategoryProducts(Category category)
        {
            return this.GetByKey(category.Id).Products;
        }

        public void Insert(Category entity)
        {
            this.context.Categories.Add(entity);
        }

        public void Update(Category entity)
        {
            var oldEntry = this.GetByKey(entity.Id);
            if (oldEntry != null)
            {
                this.context.Entry(oldEntry).CurrentValues.SetValues(entity);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        context = null;
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
