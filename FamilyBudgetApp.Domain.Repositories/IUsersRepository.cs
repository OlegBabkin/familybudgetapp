﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;
using System.Collections.Generic;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface IUsersRepository : IBaseRepository<User>, IGetByKey<User, int>
    {
        IEnumerable<UserAccount> GetUserAccountCollection(User user);
    }
}
