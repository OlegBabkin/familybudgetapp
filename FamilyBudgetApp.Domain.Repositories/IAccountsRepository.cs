﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;
using System.Collections.Generic;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface IAccountsRepository : IBaseRepository<Account>, IGetByKey<Account, int>
    {
        IEnumerable<UserAccount> GetUserAccountCollection(Account account);
    }
}
