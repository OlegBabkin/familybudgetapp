﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;
using System.Collections.Generic;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface ITransationTypesRepository : IBaseRepository<TransactionType>, IGetByKey<TransactionType, int>
    {
        IEnumerable<Transaction> GetTypeTrunsactions(TransactionType type);
    }
}
