﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;
using System.Collections.Generic;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface ICategoriesRepository : IBaseRepository<Category>, IGetByKey<Category, int>
    {
        IEnumerable<Product> GetCategoryProducts(Category category);
    }
}
