﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;
using System.Collections.Generic;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface IProductsRepository : IBaseRepository<Product>, IGetByKey<Product, int>
    {
        IEnumerable<Transaction> GetProductTransactions(Product product);
    }
}
