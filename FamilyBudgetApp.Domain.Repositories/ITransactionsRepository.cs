﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface ITransactionsRepository : IBaseRepository<Transaction>, IGetByKey<Transaction, int>
    {
    }
}
