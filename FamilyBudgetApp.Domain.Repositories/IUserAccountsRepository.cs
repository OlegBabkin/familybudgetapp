﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories.Base;
using System.Collections.Generic;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface IUserAccountsRepository : IBaseRepository<UserAccount>, IGetByKey<UserAccount, int>
    {
        IEnumerable<Transaction> GetUserAccountTransactions(UserAccount userAccount);
    }
}
