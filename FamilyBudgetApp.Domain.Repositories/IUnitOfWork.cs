﻿using System;

namespace FamilyBudgetApp.Domain.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IAccountsRepository Accounts { get; }
        ICategoriesRepository Categories { get; }
        IProductsRepository Products { get; }
        ITransactionsRepository Transactions { get; }
        ITransationTypesRepository TransactionTypes { get; }
        IUserAccountsRepository UserAccountCollection { get; }
        IUsersRepository Users { get; }

        void Save();
    }
}
