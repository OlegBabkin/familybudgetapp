﻿namespace FamilyBudgetApp.Domain.Repositories.Base
{
    public interface ICUDRepository<T> where T : class
    {
        void Insert(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
