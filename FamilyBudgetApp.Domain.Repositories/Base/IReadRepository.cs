﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FamilyBudgetApp.Domain.Repositories.Base
{
    public interface IReadRepository<T> where T : class
    {
        IEnumerable<T> GetAllEntries();
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
    }
}
