﻿namespace FamilyBudgetApp.Domain.Repositories.Base
{
    public interface IGetByKey<T, K> where T : class
    {
        T GetByKey(K key);
    }
}
