﻿using System;

namespace FamilyBudgetApp.Domain.Repositories.Base
{
    public interface IBaseRepository<T> : IReadRepository<T>, ICUDRepository<T>, IDisposable where T : class
    {

    }
}
