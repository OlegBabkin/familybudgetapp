﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Domain.Repositories;
using System.Windows;
using FamilyBudgetApp.UI.WPFApp.Views;

namespace FamilyBudgetApp.UI.WPFApp.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        public IUnitOfWork Uow { get; private set; }

        private User currentUser;
        public User CurrentUser
        {
            get { return currentUser; }
            set
            {
                currentUser = value;
                OnPropertyChanged("CurrentUser");
            }
        }

        // --------------- Properties declaration ------------------ //
        private ObservableCollection<Account> accounts;
        public ObservableCollection<Account> Accounts
        {
            get { return accounts; }
            set
            {
                accounts = value;
                OnPropertyChanged("Accounts");
            }
        }

        private ObservableCollection<Category> categories;
        public ObservableCollection<Category> Categories
        {
            get { return categories; }
            set
            {
                categories = value;
                OnPropertyChanged("Categories");
            }
        }

        private ObservableCollection<Product> products;
        public ObservableCollection<Product> Products
        {
            get { return products; }
            set
            {
                products = value;
                OnPropertyChanged("Products");
            }
        }

        private ObservableCollection<Transaction> transactions;
        public ObservableCollection<Transaction> Transactions
        {
            get { return transactions; }
            set
            {
                transactions = value;
                OnPropertyChanged("Transactions");
            }
        }

        private ObservableCollection<TransactionType> transactionTypes;
        public ObservableCollection<TransactionType> TransactionTypes
        {
            get { return transactionTypes; }
            set
            {
                transactionTypes = value;
                OnPropertyChanged("TransactionTypes");
            }
        }

        private ObservableCollection<User> users;
        public ObservableCollection<User> Users
        {
            get { return users; }
            set
            {
                users = value;
                OnPropertyChanged("Users");
            }
        }

        private ObservableCollection<UserAccount> userAccounts;
        public ObservableCollection<UserAccount> UserAccounts
        {
            get { return userAccounts; }
            set
            {
                userAccounts = value;
                OnPropertyChanged("UserAccounts");
            }
        }

        private Product selectedProduct;
        public Product SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                OnPropertyChanged("SelectedProduct");
            }
        }

        private Category selectedCategory;
        public Category SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;
                OnPropertyChanged("SelectedCategory");
            }
        }

        private Transaction selectedTransaction;
        public Transaction SelectedTransaction
        {
            get { return selectedTransaction; }
            set
            {
                selectedTransaction = value;
                OnPropertyChanged("SelectedTransaction");
            }
        }

        // Commands
        private RelayCommand saveToDatabaseCommand;
        public RelayCommand SaveToDatabaseCommand
        {
            get
            {
                return saveToDatabaseCommand ?? (saveToDatabaseCommand = new RelayCommand(obj =>
                {
                    this.Uow.Save();
                    this.Transactions = new ObservableCollection<Transaction>(Uow.Transactions.GetAllEntries());
                }));
            }
        }

        private RelayCommand removeProductCommand;
        public RelayCommand RemoveProductCommand
        {
            get
            {
                return removeProductCommand ?? (removeProductCommand = new RelayCommand(obj =>
                {
                    Product product = obj as Product;
                    if (product != null)
                    {
                        List<Transaction> transactionsList = Uow.Transactions.FindBy(tr => tr.Product.Name.Equals(product.Name)).ToList();
                        if (transactionsList.Count > 0)
                        {
                            MessageBox.Show("Продукт используется в учёте, такие продукты можно только изменять, но не удалять!");
                            return;
                        }

                        int id = product.Id;
                        Product p = Uow.Products.GetByKey(id);
                        Products.Remove(p);
                        Uow.Products.Delete(p);
                        Uow.Save();
                    }
                }));
            }
        }

        private RelayCommand removeCategoryCommand;
        public RelayCommand RemoveCategoryCommand
        {
            get
            {
                return removeCategoryCommand ?? (removeCategoryCommand = new RelayCommand(obj =>
                {
                    Category category = obj as Category;
                    if (category != null)
                    {
                        List<Product> productsList = Uow.Products.FindBy(pr => pr.Category.Name.Equals(category.Name)).ToList();
                        if (productsList.Count > 0)
                        {
                            MessageBox.Show("Данная категория используется в описи продуктов, такие категории можно только изменять, но не удалять!");
                            return;
                        }

                        int id = category.Id;
                        Category c = Uow.Categories.GetByKey(id);
                        Categories.Remove(c);
                        Uow.Categories.Delete(c);
                        Uow.Save();
                    }
                }));
            }
        }

        private RelayCommand addUserCommand;
        public RelayCommand AddUserCommand
        {
            get
            {
                return addUserCommand ?? (addUserCommand = new RelayCommand(obj =>
                {
                    CreateUserView createUserView = new CreateUserView(new User());
                    if (createUserView.ShowDialog() == true)
                    {
                        User user = createUserView.User;
                        List<User> usersList = Uow.Users.FindBy(us => us.Login.Equals(user.Login)).ToList();
                        if (usersList.Count > 0)
                        {
                            MessageBox.Show("Пользователь с таким логином уже существует!");
                            return;
                        }
                        Uow.Users.Insert(user);
                        Users.Add(user);
                        Uow.Save();
                        var users = Uow.Users.FindBy(u => u.Login.Equals(user.Login));
                        user = users.ToList()[0];
                        var publicAccounts = Uow.Accounts.FindBy(a => a.Type.Equals("Публичный"));
                        if (publicAccounts.Count() > 0)
                        {
                            foreach (var item in publicAccounts)
                            {
                                Uow.UserAccountCollection.Insert(new UserAccount { UserId = user.Id, AccountId = item.Id});
                            }
                            Uow.Save();
                        }
                        MessageBox.Show("Пользователь успешно добавлен.");
                        CurrentUser = user;
                        UserAccounts = new ObservableCollection<UserAccount>(CurrentUser.UserAccounts);
                    }
                }));
            }
        }
        // --------------- Properties declaration ends -------------- //

        // --------------- Constructor ------------------------------ //
        public MainWindowViewModel(IUnitOfWork uow)
        {
            Uow = uow;
            Accounts = new ObservableCollection<Account>(Uow.Accounts.GetAllEntries());
            Categories = new ObservableCollection<Category>(Uow.Categories.GetAllEntries());
            Products = new ObservableCollection<Product>(Uow.Products.GetAllEntries());
            Transactions = new ObservableCollection<Transaction>(Uow.Transactions.GetAllEntries());
            TransactionTypes = new ObservableCollection<TransactionType>(Uow.TransactionTypes.GetAllEntries());
            Users = new ObservableCollection<User>(Uow.Users.GetAllEntries());
            UserAccounts = new ObservableCollection<UserAccount>();
        }
    }
}
