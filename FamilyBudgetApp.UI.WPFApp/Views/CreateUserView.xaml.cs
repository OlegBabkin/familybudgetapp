﻿using FamilyBudgetApp.Domain.Core;
using System.Windows;

namespace FamilyBudgetApp.UI.WPFApp.Views
{
    /// <summary>
    /// Interaction logic for CreateUserView.xaml
    /// </summary>
    public partial class CreateUserView : Window
    {
        public User User { get; private set; }

        public CreateUserView(User user)
        {
            InitializeComponent();
            User = user;
            this.DataContext = User;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (LoginTextB.Text.Length <= 0 || NameTextB.Text.Length <= 0)
            {
                MessageBox.Show("Все поля должны быть заполнены");
                return;
            }
            this.DialogResult = true;
        }
    }
}
