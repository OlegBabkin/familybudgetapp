﻿using FamilyBudgetApp.Domain.Core;
using System.Collections.ObjectModel;
using System.Windows;

namespace FamilyBudgetApp.UI.WPFApp.Views
{
    /// <summary>
    /// Interaction logic for SelectUserView.xaml
    /// </summary>
    public partial class SelectUserView : Window
    {
        public ObservableCollection<User> Users { get; private set; }

        public SelectUserView()
        {
            InitializeComponent();
        }
    }
}
