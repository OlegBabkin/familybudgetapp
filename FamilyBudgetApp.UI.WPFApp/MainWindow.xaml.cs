﻿using FamilyBudgetApp.Domain.Core;
using FamilyBudgetApp.Infrastructure.Database;
using FamilyBudgetApp.Infrastructure.Database.Connection;
using FamilyBudgetApp.UI.WPFApp.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System;
using System.Collections.ObjectModel;
using System.Globalization;

namespace FamilyBudgetApp.UI.WPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            viewModel = new MainWindowViewModel(new UnitOfWork(new AppDbContext()));
            this.DataContext = viewModel;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime dt = DateTime.Now;
            TransDateDatePicker.SelectedDate = dt;
            TransAmountTextB.Text = "1.000";
        }

        private void SelectUserComboB_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (viewModel.CurrentUser != null)
            {
                viewModel.UserAccounts = new ObservableCollection<UserAccount>(viewModel.CurrentUser.UserAccounts);
            }
        }

        // **************** Products ****************** //
        // ******************************************** //
        // Create
        private void AddProductBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!IsProductFieldsValid()) { return; }

            Product p = new Product()
            {
                Name = addProductTextB.Text,
                Category = (Category)addProductComboBCat.SelectedItem
            };

            viewModel.Products.Add(p);
            viewModel.Uow.Products.Insert(p);
            viewModel.Uow.Save();
            MessageBox.Show("Новый продукт добавлен.");
        }

        // Update
        private void UpdateProductBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ProductListView.SelectedItem == null) { return; }

            if (!IsProductFieldsValid()) { return; }

            Product product = viewModel.Uow.Products.GetByKey(viewModel.SelectedProduct.Id);
            if (product != null)
            {
                product.Name = addProductTextB.Text;
                product.CategoryId = (int)addProductComboBCat.SelectedValue;
            };

            viewModel.Uow.Products.Update(product);
            viewModel.Uow.Save();
            int index = viewModel.Products.IndexOf(viewModel.SelectedProduct);
            viewModel.Products.Remove(viewModel.SelectedProduct);
            viewModel.Products.Insert(index, product);
            MessageBox.Show("Продукт обновлён.");
        }

        private bool IsProductFieldsValid()
        {
            List<Product> productsList = viewModel.Uow.Products.FindBy(pr => pr.Name.Equals(addProductTextB.Text)).ToList();
            if (productsList.Count > 0)
            {
                MessageBox.Show("Продукт с таким именем уже существует!");
                return false;
            }

            if (addProductTextB.Text.Length == 0 || addProductComboBCat.Text.Length == 0)
            {
                MessageBox.Show("Все поля должны быть заполнены");
                return false;
            }
            return true;
        }
        // ******************************************** //

        // **************** Categories **************** //
        // ******************************************** //
        private void AddCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!IsCategoryFieldValid()) { return; }

            Category category = new Category() { Name = AddCategoryTextB.Text };

            viewModel.Categories.Add(category);
            viewModel.Uow.Categories.Insert(category);
            viewModel.Uow.Save();
            MessageBox.Show("Новая Категория добавлена.");
        }

        private void UpdateCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CategoriesListBox.SelectedItem == null) { return; }

            if (!IsCategoryFieldValid()) { return; }

            Category category = viewModel.Uow.Categories.GetByKey(viewModel.SelectedCategory.Id);
            if (category != null) { category.Name = AddCategoryTextB.Text; };

            viewModel.Uow.Categories.Update(category);
            viewModel.Uow.Save();
            int index = viewModel.Categories.IndexOf(viewModel.SelectedCategory);
            viewModel.Categories.Remove(viewModel.SelectedCategory);
            viewModel.Categories.Insert(index, category);
            MessageBox.Show("Категория обновлена.");
        }

        private bool IsCategoryFieldValid()
        {
            if (AddCategoryTextB.Text.Length == 0)
            {
                MessageBox.Show("Поле для ввода категории не должно быть пустым");
                return false;
            }

            List<Category> categoriesList = viewModel.Uow.Categories.FindBy(c => c.Name.Equals(AddCategoryTextB.Text)).ToList();
            if (categoriesList.Count > 0)
            {
                MessageBox.Show("Категория с таким именем уже существует!");
                return false;
            }
            return true;
        }
        // ******************************************** //

        // *************** Transactions *************** //
        // ******************************************** //
        private void UpdateSummTextB()
        {
            if (TransPriceTextB.Text.Length > 0 && TransAmountTextB.Text.Length > 0)
            {
                TransSummTextB.Text = (Convert.ToDouble(TransPriceTextB.Text, CultureInfo.InvariantCulture) * 
                    Convert.ToDouble(TransAmountTextB.Text, CultureInfo.InvariantCulture)).ToString();
            }
        }

        private void TransPriceTextB_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateSummTextB();
        }

        private void TransAmountTextB_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateSummTextB();
        }

        private void AddTransBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!IsTransactionFieldsValid()) { return; }
            Decimal price = ((TransactionType)TransTypeComboB.SelectedItem).Name.Equals("Расходы") ?
                    Convert.ToDecimal(TransPriceTextB.Text, CultureInfo.InvariantCulture) * (-1) :
                    Convert.ToDecimal(TransPriceTextB.Text, CultureInfo.InvariantCulture) * 1;

            Transaction transaction = new Transaction
            {
                Date = TransDateDatePicker.SelectedDate,
                Product = (Product) TransProductComboB.SelectedItem,
                TransactionType = (TransactionType) TransTypeComboB.SelectedItem,
                EuroValue = price,
                Amount = Convert.ToDecimal(TransAmountTextB.Text, CultureInfo.InvariantCulture),
                UserAccount = (UserAccount) TransAccountComboB.SelectedItem
            };

            Account acc = transaction.UserAccount.Account;
            int index = viewModel.Accounts.IndexOf(acc);
            viewModel.Accounts.Remove(acc);
            acc.Balance += transaction.EuroValue * transaction.Amount;
            viewModel.Uow.Accounts.Update(acc);
            viewModel.Accounts.Insert(index, acc);

            viewModel.Uow.Transactions.Insert(transaction);
            viewModel.Uow.Save();
            viewModel.Transactions.Add(transaction);
        }

        private void DeleteTransBtn_Click(object sender, RoutedEventArgs e)
        {
            int itemsCount = TransDataGrid.SelectedItems.Count;
            if (itemsCount > 0)
            {
                for (int i = 0; i < itemsCount; i++)
                {
                    Transaction tr = TransDataGrid.SelectedItems[i] as Transaction;
                    if (tr != null)
                    {
                        viewModel.Uow.Transactions.Delete(tr);
                        viewModel.Transactions.Remove(tr);
                    }
                }
            }
            viewModel.Uow.Save();
            MessageBox.Show("Удалено " + itemsCount + " записей.");
        }

        private void UpdateTransBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private bool IsTransactionFieldsValid()
        {
            if (!TransDateDatePicker.SelectedDate.HasValue || 
                TransProductComboB.Text.Length <= 0        ||
                TransTypeComboB.Text.Length <= 0           ||
                TransPriceTextB.Text.Length <= 0           ||
                TransAmountTextB.Text.Length <= 0          ||
                TransAccountComboB.Text.Length <= 0)
            {
                MessageBox.Show("Все поля должны бытьзаполнены!");
                return false;
            }

            return true;
        }

        private void Srch_btn_Click_1(object sender, RoutedEventArgs e)
        {
            if (Srch_TextB.Text.Length == 0 && SrcCatComboB.Text.Length == 0)
            {
                viewModel.Transactions = new ObservableCollection<Transaction>(viewModel.Uow.Transactions.GetAllEntries());
            }
            if (SrcCatComboB.Text.Length == 0 && Srch_TextB.Text.Length != 0)
            {
                viewModel.Transactions = new ObservableCollection<Transaction>(viewModel.Uow.Transactions.FindBy(t => t.Product.Name.Contains(Srch_TextB.Text))); //productDataGrid.ItemsSource = uow.Products.FindBy(p => p.Name.Contains(srch_tb.Text)).ToList();
            }
            if (SrcCatComboB.Text.Length != 0 && Srch_TextB.Text.Length == 0)
            {
                viewModel.Transactions = new ObservableCollection<Transaction>(viewModel.Uow.Transactions.FindBy(t => t.Product.Category.Name.Contains(SrcCatComboB.Text))); //productDataGrid.ItemsSource = uow.Products.FindBy(p => p.Category.Name.Contains(srch_cat_cb.Text)).ToList();
            }
            if (SrcCatComboB.Text.Length != 0 && Srch_TextB.Text.Length != 0)
            {
                viewModel.Transactions = new ObservableCollection<Transaction>(viewModel.Uow.Transactions.FindBy(t => t.Product.Category.Name.Contains(SrcCatComboB.Text) && t.Product.Name.Contains(Srch_TextB.Text))); //productDataGrid.ItemsSource = uow.Products.FindBy(p => p.Category.Name.Contains(srch_cat_cb.Text) && p.Name.Contains(srch_tb.Text)).ToList();
            }
        }
    }
}
